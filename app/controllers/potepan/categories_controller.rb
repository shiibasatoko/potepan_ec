class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.eager_load(:root, taxons: :children).all
    @products = @taxon.all_products.includes(master: [:default_price, :images])
  end
end
