require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET /show" do
    let(:taxonomy) { create(:taxonomy, taxons: [taxon]) }
    let(:taxon) { create(:taxon, products: [product]) }
    let(:product) { create(:product) }

    before do
      get potepan_category_path(taxon.id)
    end

    it 'リクエストが成功すること' do
      expect(response).to have_http_status(200)
    end

    it '期待されているtaxon名が返ってくること' do
      expect(response.body).to include taxon.name
    end

    it '期待されている各カテゴリの商品名が返ってくること' do
      expect(response.body).to include product.name
    end
  end
end
