require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET #show" do
    let(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon) }
    let!(:related_product) { create(:product, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it '正常にレスポンスを返すこと' do
      expect(response).to be_successful
    end

    it 'showテンプレートで表示されること' do
      expect(response).to have_http_status(200)
    end

    it "@productが期待される値を持つこと" do
      expect(assigns(:product)).to eq(product)
    end

    it "関連商品が取得できていること" do
      expect(product.related_products).to include(*assigns(:related_products))
    end
  end
end
