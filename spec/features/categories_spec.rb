require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:taxonomy) { create(:taxonomy, taxons: [taxon]) }
  let(:taxon) { create(:taxon, products: [product]) }
  let(:product) { create(:product) }
  let!(:other_taxonomy) { create(:taxonomy) }
  let!(:other_taxon) { create(:taxon, parent: other_taxonomy.root) }
  let!(:other_product) { create(:product, taxons: [other_taxon]) }

  before do
    visit potepan_category_path(id: taxon.id)
  end

  scenario 'カテゴリーページが正しく表示されていること' do
    expect(page).to have_content taxon.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  scenario 'サイドバーが正しく表示されていること' do
    within ".side-nav" do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxon.name
      expect(page).to have_content taxon.product_ids.count
      expect(page).to have_link taxon.name
      expect(page).to have_content other_taxonomy.name
      expect(page).to have_content other_taxon.name
      expect(page).to have_content other_taxon.product_ids.count
      expect(page).to have_link other_taxon.name
    end
  end

  scenario 'カテゴリーをclickすると、clickしたカテゴリーに紐づく商品のみ表示されていること' do
    expect(page).to have_link other_taxon.name
    click_on other_taxon.name
    expect(current_path).to eq potepan_category_path(other_taxon.id)
  end

  scenario 'カテゴリーページから商品詳細ページへ移動すること' do
    expect(page).to have_link product.name
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
