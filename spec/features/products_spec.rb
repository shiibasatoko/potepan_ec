require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:product) { create(:product, taxons: [taxon], price: 123) }
  let(:taxon) { create(:taxon) }
  let!(:other_product) { create(:product, taxons: [taxon], price: 321) }

  before do
    visit potepan_product_path(id: product.id)
  end

  scenario '商品詳細ページにアクセスする' do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
  end

  scenario '商品詳細ページからカテゴリーページへ移動すること' do
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(product.taxons.first.id)
  end

  scenario "関連商品が正しく表示されていること。" do
    within('.productBox') do
      expect(page).not_to have_content product.name
      expect(page).not_to have_content "$123"
      expect(page).to have_content other_product.name
      expect(page).to have_content "$321"
    end
  end

  scenario "関連商品をclickすると詳細ページへ移動すること。" do
    click_on other_product.name
    expect(current_path).to eq potepan_product_path(other_product.id)
  end
end
