require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  let(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }
  let!(:unrelated_product) { create(:product) }

  describe "#related_products" do
    it "related_productsメソッドで、関連商品を取得すること" do
      expect(product.related_products).to include related_product
    end

    it "関連商品に、取得している商品を含めないこと" do
      expect(product.related_products).not_to include product
    end

    it "関連商品に、関連しない商品を含めないこと" do
      expect(product.related_products).not_to include unrelated_product
    end
  end
end
